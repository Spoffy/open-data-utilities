import argparse

PREFIXES = """
PREFIX soton: <http://id.southampton.ac.uk/ns/>
PREFIX foaf: <http://xmlns.com/foaf/0.1/>
PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
PREFIX geo: <http://www.w3.org/2003/01/geo/wgs84_pos#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX org: <http://www.w3.org/ns/org#>
PREFIX prov: <http://www.w3.org/ns/prov#>
PREFIX spacerel: <http://data.ordnancesurvey.co.uk/ontology/spatialrelations/>
PREFIX postcode: <http://data.ordnancesurvey.co.uk/ontology/postcode/>
PREFIX ep: <http://eprints.org/ontology/>
PREFIX dct: <http://purl.org/dc/terms/>
PREFIX bibo: <http://purl.org/ontology/bibo/>
PREFIX portals: <http://purl.org/openorg/portals/>
PREFIX owl: <http://www.w3.org/2002/07/owl#>
PREFIX oo: <http://purl.org/openorg/>
PREFIX rooms: <http://vocab.deri.ie/rooms#>
PREFIX goodrelations: <http://purl.org/goodrelations/v1>
PREFIX transport: <http://transport.data.gov.uk/def/naptan/>
PREFIX transit: <http://vocab.org/transit/terms/>
"""

def position_to_id(pos):
	id = ""
	for i in range(0, pos+1):
		id += "a"
	return id
	
def find_missing_data_query(restricting_expression, data_predicates, limit):
	def data_preds_to_optional():
		return "\n".join(["OPTIONAL { ?instance " + data_predicates[i] + " ?" + position_to_id(i) + " }" for i in range(0, len(data_predicates))])
	print(data_preds_to_optional())
	
	def filter():
		return (" || ".join(["(! BOUND(?{0}))".format(position_to_id(i)) for i in range(0, len(data_predicates))]))
	print(filter())

	query = """
SELECT DISTINCT ?title ?instance WHERE {{
	{restriction} .
	?instance rdf:type ?type .
OPTIONAL {{ ?instance rdfs:label ?title }}
		  
	{optional} .
	FILTER( {filter} )
}} 
ORDER BY ?type
LIMIT {limit}
	""".format(restriction=restricting_expression, optional=data_preds_to_optional(), filter=filter(), limit=limit)
	
	return PREFIXES + query;

def main(args):
    #print(find_missing_data_query("?instance a soton:UoSBuilding", ["geo:lat", "geo:long"], 400))
    print(find_missing_data_query(args.restriction, args.predicates, args.limit))
    
if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Generates a SPARQL UNION of triples, one for each class in the class file, of the format [VARIABLE_BINDING] [PREDICATE] [CLASS]")   
    
    parser.add_argument('restriction', help="Single triple used to limit the results. E.g '?instance a soton:UoSBuilding'")
    parser.add_argument('predicates', help="List of predicates that should be checked if they're missing", nargs="+")
    parser.add_argument('--limit', help="Number of results to return", default=400)
    
    args = parser.parse_args()
    main(args)