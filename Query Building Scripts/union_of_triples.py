import argparse

def load_file(file_name):
	with open(file_name, "r") as input:
		return [line[0:-1] for line in input.readlines()]

def union_of_triples(classes, variable_binding, predicate):
	query_prefix = variable_binding + " " + predicate
	return "{ " + query_prefix + " <" + ("> } UNION { " + query_prefix + " <").join(classes) + "> }"
    
def main(args):
    classes = load_file(args.class_file)
    print(union_of_triples(classes, args.variable_binding, args.predicate))

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Generates a SPARQL UNION of triples, one for each class in the class file, of the format [VARIABLE_BINDING] [PREDICATE] [CLASS]")   
    
    parser.add_argument('class_file', help="A file containing a newline seperated list of RDF classes")
    parser.add_argument('--variable_binding', help="Optional name of the variable used as the triple's subject", default="?instance")
    parser.add_argument('--predicate', help="Predicate used to associate variable binding with union", default="rdf:type")
    
    args = parser.parse_args()
    main(args)